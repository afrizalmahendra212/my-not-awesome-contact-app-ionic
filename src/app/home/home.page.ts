import { Component } from '@angular/core';
import { HttpClient,HttpHeaders } from '@angular/common/http';
import { ToastController, NavController } from '@ionic/angular';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

  public data : any;
  public contact : any;

  constructor(
    public http:HttpClient,
    public navCtrl : NavController,
    public toas : ToastController
  ){
    this.viewData();
  }

  viewData(){
    this.http.get("http://localhost/contact/tampil.php")
    .subscribe(res=>{
      this.contact = this.data = res;
      console.log(res);
    });
  }

  addData(){
    this.navCtrl.navigateBack('/add-contact').then(()=>{
      this.viewData();
    });
  }

  singleData(data){
    console.log(data);
    this.navCtrl.navigateBack('/view-contact/'+JSON.stringify(data));
  }

  filterData(e){
    console.log(e);
    if(e!=""){
      this.data = this.data.filter((item) => {
        return item.Nama.toLowerCase().indexOf(e.toLowerCase()) > -1;
      }); 
    } else {
      this.data = this.contact;
    }
  }

  Edit(item){
    this.navCtrl.navigateBack('/add-contact/'+JSON.stringify(item));
  }

  Delete(id){
    // console.log(id)

    this.http.get(`http://localhost/contact/delete.php?id=${id}`)
    .subscribe(res=>{
      console.log(res);
        if(res['type']=="success"){
          this.viewData()
          this.notif(res['mess']);
        } else {
          this.notif(res['mess']);
        }
    });
  }

  async notif(mess) {
    const toast = await this.toas.create({
      message: mess,
      showCloseButton: true,
      position: 'bottom',
      duration:1000
    });
    toast.present();
    toast.onDidDismiss().then(()=>{
    });
  }

}
