import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  { path: 'home', loadChildren: './home/home.module#HomePageModule' },
  { path: 'add-contact', loadChildren: './add-contact/add-contact.module#AddContactPageModule' },
  { path: 'add-contact/:data', loadChildren: './add-contact/add-contact.module#AddContactPageModule' },
  { path: 'view-contact/:data', loadChildren: './view-contact/view-contact.module#ViewContactPageModule' },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
