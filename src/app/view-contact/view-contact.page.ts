import { Component, OnInit } from '@angular/core';
import { NavController, NavParams } from '@ionic/angular';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-view-contact',
  templateUrl: './view-contact.page.html',
  styleUrls: ['./view-contact.page.scss'],
})
export class ViewContactPage implements OnInit {

  public data : any;

  constructor(
    public navCtrl : NavController,
    public route : ActivatedRoute
  ){
    this.data =  JSON.parse(this.route.snapshot.paramMap.get("data"));
    console.log(this.data);
  }

  ngOnInit() {
  }

}
