import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ToastController, NavController } from '@ionic/angular';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-add-contact',
  templateUrl: './add-contact.page.html',
  styleUrls: ['./add-contact.page.scss'],
})
export class AddContactPage implements OnInit {

  id: any = ""; nama: any; telp: any; email: any; data: any;

  constructor(
    public navCtrl: NavController,
    public http: HttpClient,
    public toas: ToastController,
    public route: ActivatedRoute
  ) {

    this.data = JSON.parse(this.route.snapshot.paramMap.get("data"));

    if (this.data != null) {
      this.id = this.data.ID
      this.nama = this.data.Nama
      this.email = this.data.Email
      this.telp = this.data.Telp
    }
  }

  ngOnInit() {
  }

  save() {
    let opt: any
    opt = new HttpHeaders();
    opt.append('Accept', 'application/json');
    opt.append('Content-Type', 'application/json');

    let frmContact = new FormData();
    frmContact.append('id', this.id);
    frmContact.append('nama', this.nama);
    frmContact.append('telp', this.telp);
    frmContact.append('email', this.email);

    let url;

    if (this.data != null) {
      url = "http://localhost/contact/add.php"
    } else {
      url = "http://localhost/contact/edit.php"
    }

      this.http.post(url, frmContact, opt)
        .subscribe(res => {
          console.log(res);
          if (res['type'] == "success") {
            this.notif(res['mess']);
          } else {
            this.notif(res['mess']);
          }
        });
  }

  async notif(mess) {
    const toast = await this.toas.create({
      message: mess,
      showCloseButton: true,
      position: 'bottom',
      duration: 1000
    });
    toast.present();
    toast.onDidDismiss().then(() => {
      this.navCtrl.back();
    });
  }

}
